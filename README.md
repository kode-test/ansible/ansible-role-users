Role Name
=========

Adding new users

Role Variables
--------------
```
user_sudo_on: true | Add user to sudoers file so they can run sudo commands
user_details:
      - {name: 'testuser1', groups: [''], password: '123'} | param for new users
```


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - users

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
